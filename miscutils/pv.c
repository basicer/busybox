/* vi: set sw=4 ts=4: */
/*
 * pv implementation for busybox
 *
 * Copyright (C) 2011  Robert Blanckaert  <basicer@basicer.com>
 *
 * Origional pv implementation by Andrew Wood
 * <http://www.ivarch.com/programs/pv.shtml>
 *
 * Licensed under GPLv2, see file LICENSE in this source tree.
 */

//kbuild:lib-$(CONFIG_PV)     += pv.o

//applet:IF_PV(APPLET(pv, BB_DIR_USR_BIN, BB_SUID_DROP))

//config:config PV
//config:	bool "pv (Pipe Viewer)"
//config:	default y
//config:	help
//config:	  pv allows a user to see the progress of data through a
//config:	  pipeline, giving information such as time elapsed, 
//config:	  percentage completed (with progress bar), current
//config:	  throughput rate, total data transferred, and ETA.
//config:config FEATURE_PV_BAR_OPTIONS
//config:	bool "  Fancy Menu Bar"
//config:	default y if DESKTOP
//config:	help
//config:	  PV Supports a few options to control its output bar.
//config:	  We need to implement our own progress bar drawing code
//config:	  to support this.  Turning this off will use the progress
//config:	  bar drawing code in libbb.
//config:config FEATURE_PV_ADVANCED_FD_COPY
//config:	bool "  Advanced FD Copy"
//config:	default n
//config:	help
//config:	  A more advanced version of bb_copyfd_size will be used.  This
//config:	  allows us to update the progress bar while transferes have 
//config:	  stalled, and also enables the use of the --wait option.

//usage:#define pv_trivial_usage
//usage:       "[OPTION] [FILE]...\n"
//usage:#define pv_full_usage "\n\n"
//usage:       "Interactive pipe thoughput viewer\n"
//usage:     IF_FEATURE_PV_BAR_OPTIONS(
//usage:     "\n    -p  Show ASCII art progress bar"
//usage:     "\n    -t  Display current running time"
//usage:     "\n    -e  Show a guess of whne the transfer will finish"
//usage:     "\n    -r  Display current trnsfer rate"
//usage:     "\n    -b  Dispaly number of bytes transfered so far"
//usage:     )
//usage:     "\n    -n  Instead of an interactive bar, print percent transfered to STDOUT"
//usage:
//usage:#define pv_example_usage
//usage:       "$ pv bigfile > /dev/null\n"
//usage:       "1.7gb 0:00:02 [912mb/s] [=======>                  ] 29% ETA 0:00:04"

#include "libbb.h"

static const char pv_options[] ALIGN1 = 
	"nqWs:i:fchV"
	IF_FEATURE_PV_BAR_OPTIONS("pterbw:H:N:")
;

static const char pv_options_comp[] ALIGN1 = 
	"h-:V-"
	IF_FEATURE_PV_BAR_OPTIONS(":w+:H+")
;
enum {
	OPT_NUMERIC   = (1 << 0),
	OPT_QUIET     = (1 << 1),
	OPT_WAIT      = (1 << 2),
	OPT_SIZE      = (1 << 3),
	OPT_INTERVAL  = (1 << 4),
	OPT_FORCE     = (1 << 5),
	OPT_CURSOR    = (1 << 6),
	OPT_HELP      = (1 << 7),
	OPT_VERSION   = (1 << 8),
	OPT_PROGRESS  = (1 << 9),
	OPT_TIMER     = (1 << 10),
	OPT_ETA       = (1 << 11),
	OPT_RATE      = (1 << 12),
	OPT_BYTES     = (1 << 13),
	OPT_WIDTH     = (1 << 14),
	OPT_HEIGHT    = (1 << 15),
	OPT_NAME      = (1 << 16),
};


struct globals {
IF_FEATURE_PV_BAR_OPTIONS(
	bool show_progress;
	bool show_timer;
	bool show_eta;
	bool show_rate;
	bool show_bytes;
	char *name;
	char *line;
)
	bool numeric;
	bool quiet;
	bool force;
	bool wait;
	bool cursor;
	bool write_stats;
	bool splice;
	unsigned interval; //in ms;
	unsigned console_width;
	unsigned console_height;
} FIX_ALIASING;


#define G (*(struct globals*)&bb_common_bufsiz1)



static const char pv_longopts[] ALIGN1 =
IF_FEATURE_PV_BAR_OPTIONS(
	            "progress\0" No_argument "p"
	            "timer\0" No_argument "t"
	            "eta\0" No_argument "e"
	            "rate\0" No_argument "r"
	            "bytes\0" No_argument "b"
)
	            "numeric\0" No_argument "n"
	            "quiet\0" No_argument "q"
	            "wait\0" No_argument "W"
	            "size\0" Required_argument "s"
	            "interval\0" Required_argument "i"
	            "width\0" Required_argument "w"
	            "height\0" Required_argument "H"
	            "name\0" Required_argument "N"
	            "force\0" No_argument "f"
	            "cursor\0" No_argument "c"

	            ;

struct stream_stats {
	off_t bytes_written;
	off_t bytes_total;
	off_t rate;

	unsigned long long start_time; //When we started streaming in us
	unsigned long long last_time;  //When the last copy completed

	unsigned elapsed;
	unsigned eta;
	int percent;
};

#if ENABLE_FEATURE_PV_BAR_OPTIONS
const char *byte_units =  " kmgtpezy";
void write_status_line(struct stream_stats *stats)
{
	size_t right_size = 0;
	int line_area = 0;

	float progress = 0.1f;

	char *lptr = NULL;
	char *rptr = NULL;
	char mline[G.console_width + 2];
	G.line = mline;
	memset(G.line+1, ' ', G.console_width);
	G.line[0] = '\r';
	char *end = G.line + G.console_width;
	char b_str[5] = { 0, 0, 0, 0, 0 };

	smart_ulltoa4(stats->rate, b_str, byte_units);
	lptr = G.line + 1;
	*(lptr++) = ' ';
	if ( G.name != NULL && lptr < end ) {
	    lptr += snprintf(lptr, end - lptr, "%8s: ", G.name);
	}
	if ( G.show_bytes && lptr < end ) {
		smart_ulltoa4(stats->bytes_written, b_str, byte_units);
	    lptr += snprintf(lptr, end - lptr, "%sb ", b_str);
	}

	if ( G.show_timer && lptr < end ) {
		lptr += snprintf(lptr, end - lptr, "%d:%02d:%02d ",
			stats->elapsed / 3600, (stats->elapsed / 60) % 60, stats->elapsed %60);
	}

	if ( G.show_rate && lptr < end ) {
		smart_ulltoa4(stats->rate, b_str, byte_units);
	    lptr += snprintf(lptr, end - lptr, "[%sb/s] ", b_str);
	}

	rptr = lptr;

	if ( G.show_progress && rptr < end ) {
		rptr += snprintf(rptr, end - rptr, " %02d%%", stats->percent);
	}

	if ( G.show_eta && rptr < end ) {
		rptr += snprintf(rptr, end - rptr, " ETA %d:%02d:%02d",
			stats->eta / 3600, (stats->eta / 60) % 60, stats->eta %60);
	}

	right_size = (rptr-lptr);
	line_area = G.console_width - (lptr - G.line) - right_size - 3;
	if ( G.show_progress && rptr < end && line_area > 5) {
		memmove(end - right_size, lptr, right_size);
		*lptr = '[';
		memset(lptr+1,' ',line_area);

		if ( stats->bytes_total > 0 ) { 
			float progress = (float)stats->bytes_written/(float)stats->bytes_total;
			int to_draw = line_area * progress;
			memset(lptr+1,'=',to_draw);
			*(lptr+(1+to_draw)) = '>';
			memset(lptr+to_draw+2,' ',line_area - to_draw);
		} else {
			int base = (line_area-3);
			int x = ((int)(stats->last_time - stats->start_time) / 1000 / G.interval) % (base * 2);
			x = ((x > base ? 2*base : 0 ) + (x>base ? -1 : 1) * x);
			*(lptr+(1+x)) = '<';
			*(lptr+(2+x)) = '=';
			*(lptr+(3+x)) = '>';
		}


		*(end-right_size-1) = ']';
	} else {
		if ( end > rptr ) memset(rptr,'.',end - rptr); //TODO: Double check this ammount.
	}

	*end = '\r';
	*(end+1) = '\0';
	xwrite_str(2, G.line);
}
#endif //FEATURE_PV_BAR_OPTIONS

char ** init_pv(char **argv)
{
	uint32_t opt;
	//"pterbnqWs:i:w:H:N:fichV"
	char *opt_s;
	char *opt_i;
IF_FEATURE_PV_BAR_OPTIONS(
	unsigned opt_w;
	unsigned opt_H;
	char *opt_N;
)

	memset(&G, 0, sizeof(G));
	applet_long_options = pv_longopts;
	opt_complementary = pv_options_comp;
	opt = getopt32(argv, pv_options, &opt_s, &opt_i
#if ENABLE_FEATURE_PV_BAR_OPTIONS
			,&opt_w, &opt_H, &opt_N
#endif
		);


	argv += optind;
	G.splice = true;
	G.write_stats = true;
	if ( !get_terminal_width_height(1, NULL, NULL) ) {
		xwrite_str(2, "stdin appears to be a terminal, isnt that odd ?\n\n");
	}


	/* Defaults assumed by PV */
	G.console_width = 80;
	G.console_height = 25;

	G.interval = 500; //0.5s
	
	if ( get_terminal_width_height(2, &G.console_width, &G.console_height) ) {
		//If STDERR isnt a terminal, generally we dont want to flood it with bytes.
		if ( !( opt & OPT_FORCE ) ) G.write_stats = false;
	}

	G.wait = ( opt & OPT_WAIT );
	
#if ENABLE_FEATURE_PV_BAR_OPTIONS
	if ( opt & OPT_WIDTH ) {
		G.console_width = opt_w;
	}
	if ( opt & OPT_HEIGHT ) {
		G.console_height = opt_H;
	}

	if ( opt & OPT_NAME ) {
		G.name = opt_N;
	}
	
	//Hopefully the only Malloc in the applet.
	//G.line = xmalloc(sizeof(char) * (G.console_width + 2));

	if ( opt & ( OPT_PROGRESS | OPT_TIMER | OPT_ETA | OPT_BYTES | OPT_RATE ) ) {
		G.show_progress = opt & OPT_PROGRESS;
		G.show_timer = opt & OPT_TIMER;
		G.show_eta = opt & OPT_ETA;
		G.show_bytes = opt & OPT_BYTES;
		G.show_rate = opt & OPT_RATE;
	} else {
		G.show_progress = true;
		G.show_timer = true;
		G.show_eta = true;
		G.show_bytes = true;
		G.show_rate = true;
	}
#endif
	G.numeric = opt & OPT_NUMERIC;

	return argv;
}


int pv_main(int argc, char **argv) MAIN_EXTERNALLY_VISIBLE;
int pv_main(int argc UNUSED_PARAM, char **argv)
{
	int fd;
	int retval = EXIT_SUCCESS;
	struct stream_stats stats;

	//TODO: Check if sizeof(globals) < BUF_SIZE
	argv = init_pv(argv);
	memset(&stats, 0, sizeof(stats));


	char **file;
	struct stat stat_buf;
	unsigned long long now;
IF_NOT_FEATURE_PV_BAR_OPTIONS(
	bb_progress_t pmt;			
)
	if (!*argv)
		argv = (char**) &bb_argv_dash;


	/* Estimate Size of bytes to write */
	file = argv;
	do {
		if (*file != bb_msg_standard_input && NOT_LONE_DASH(*file) ) {
			xstat(*file, &stat_buf);
			stats.bytes_total += stat_buf.st_size;	
		} else {
			/* We dont know how much STDIN will write, so give up. */
			stats.bytes_total = 0;
			break;
		}

	} while(*++file);

	file = argv;
	stats.start_time = monotonic_us();
	do {
IF_NOT_FEATURE_PV_BAR_OPTIONS(
		bb_progress_init(&pmt, *argv);
)
		fd = open_or_warn_stdin(*argv);
		if (fd >= 0) {
			off_t r;
			do {
				/* This is not a xfunc - never exits */
				if ( ENABLE_FEATURE_PV_ADVANCED_FD_COPY && G.splice ) {
					r = splice(fd, NULL, STDOUT_FILENO, NULL, 1 << 30, SPLICE_F_NONBLOCK);
					if ( r < 0 ) {
						if ( errno == EAGAIN || errno == EWOULDBLOCK ) {
							usleep(100);
						} else {
							G.splice = false;
						}
					}
				} else {
					r = bb_copyfd_size(fd, STDOUT_FILENO, 1024); //THIS IS REALLY SMALL :[

				}

				stats.bytes_written += r;
				now =  monotonic_us();		
				if ( now - stats.last_time >= 1000 * G.interval ) {
					stats.last_time = now;
					stats.elapsed = ((stats.last_time - stats.start_time) / 1000 / 1000);
					stats.rate = stats.bytes_written / (stats.elapsed > 0 ? stats.elapsed : 1);
					if ( stats.bytes_total > 0 ) {
						stats.eta = (stats.bytes_total - stats.bytes_written) / stats.rate;
						if ( stats.bytes_written == stats.bytes_total ) {
							stats.percent = 100;
						} else {
							stats.percent = (int)(100 * stats.bytes_written / stats.bytes_total);
						}
					}
					if ( G.numeric ) {
						fprintf(stderr, "% 2d\n", stats.percent);
					} else if ( G.write_stats && ( !G.wait || stats.bytes_written > 0 ))  {
IF_FEATURE_PV_BAR_OPTIONS(
                   write_status_line(&stats);
)
IF_NOT_FEATURE_PV_BAR_OPTIONS(
                   bb_progress_update(&pmt, 0, stats.bytes_written, stats.bytes_total > 0 ? stats.bytes_total : stats.bytes_written * 1.5);
)
					}
				}
			} while ( r != 0 );
			if (fd != STDIN_FILENO)
				close(fd);
			if (r >= 0)
				continue;


IF_FEATURE_PV_BAR_OPTIONS(
           write_status_line(&stats);
)
IF_NOT_FEATURE_PV_BAR_OPTIONS(
           bb_progress_update(&pmt, 0, stats.bytes_written, stats.bytes_total > 0 ? stats.bytes_total : stats.bytes_written);
)
		}
		retval = EXIT_FAILURE;
	} while (*++file);
	//free(G.line);
	xwrite_str(2,"\n");
	return retval;
}
